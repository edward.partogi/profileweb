from django.urls import path
from myProfile import views

urlpatterns = [
    path("contacts/", views.contacts, name='tes'),
    path("more/", views.more, name='more'),
    path("form/", views.form, name='form'),
    path("", views.home, name='home'),
    ]
